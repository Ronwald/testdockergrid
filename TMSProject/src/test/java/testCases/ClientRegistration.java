package testCases;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.List;

import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import pageObjects.Deliver_Home;
import utilities.MyUtilities;

public class ClientRegistration extends MyUtilities {
	

	XSSFWorkbook workbook;
    XSSFSheet sheet;
    XSSFCell cell;
    String testSite = "https://dp-test.urbanfox.asia/p/public/tracking?t_id=";
    String driverLoc = "/usr/bin/chromedriver";
    String testDataLoc = "/Test_Data_TMS.xlsx";
	
    
	@Test(priority = 1)
	public void trackDeliveryGrid1() throws IOException {
			
			ExtentTest test = extent.createTest("TESTCASE 1 : Test Client Registration - TestCase 1");
			
			// Import excel sheet.
			
			String projectDir = System.getProperty("user.dir");
			String fileLocation = projectDir + testDataLoc ;
			
			System.out.println(fileLocation);
		
			File src=new File(fileLocation); 
			// Load the file.
			FileInputStream fis = new FileInputStream(src);
			// Load he workbook.
			workbook = new XSSFWorkbook(fis);
			// Load the sheet in which data is stored.
			sheet= workbook.getSheet("Test Data TMS");
		
			//define driver capability for Chrome
			DesiredCapabilities dc=DesiredCapabilities.chrome();
			//this is the url of the Selenium Grid Hub, with exposed port 4444
			URL url = new URL("http://localhost:4444/wd/hub");
			RemoteWebDriver driver = new RemoteWebDriver(url,dc);
			//launch the test url
			driver.get(testSite);
				
			for(int i = 1;i<=sheet.getLastRowNum();i++) {
				
				cell = sheet.getRow(i).getCell(0);
				
				if(cell==null) {
					driver.quit();//close the driver if there is no more data to fetch			
				}else {
					cell.setCellType(CellType.STRING);
					String doNumber = cell.getStringCellValue();
					System.out.println(doNumber);
					
					Deliver_Home.EnterTextByClassName(driver, "form-control", doNumber, test);
					myWait(2000);
					Deliver_Home.ClickButton(driver, "Track Item", test);
					myWait(2000);
					
					
					//verify results are displayed
					Deliver_Home.verifyElementPresent(driver, ".panel--tracking__header", test);
					
					myWait(1000);
					
					Deliver_Home.ClearText(driver, "form-control", test);
					myWait(1000);
					
				}
				
			
			}
			

	}
	
	@Test(priority = 2)
	public void trackDeliveryGrid2() throws IOException {
			
			ExtentTest test = extent.createTest("TESTCASE 2 : Test Client Registration - TestCase 2");
			
			// Import excel sheet.
			
			String projectDir = System.getProperty("user.dir");
			String fileLocation = projectDir + testDataLoc ;
			
			System.out.println(fileLocation);
		
			File src=new File(fileLocation); 
			// Load the file.
			FileInputStream fis = new FileInputStream(src);
			// Load he workbook.
			workbook = new XSSFWorkbook(fis);
			// Load the sheet in which data is stored.
			sheet= workbook.getSheet("Test Data TMS");
		
			
			DesiredCapabilities dc=DesiredCapabilities.chrome();
			URL url = new URL("http://localhost:4444/wd/hub");
			RemoteWebDriver driver = new RemoteWebDriver(url,dc);
			driver.get(testSite);
				
			for(int i = 1;i<=sheet.getLastRowNum();i++) {
				
				cell = sheet.getRow(i).getCell(0);
				
				if(cell==null) {
					driver.quit();//close the driver if there is no more data to fetch			
				}else {
					cell.setCellType(CellType.STRING);
					String doNumber = cell.getStringCellValue();
					System.out.println(doNumber);
					
					Deliver_Home.EnterTextByClassName(driver, "form-control", doNumber, test);
					myWait(2000);
					Deliver_Home.ClickButton(driver, "Track Item", test);
					myWait(2000);
					
					
					//verify results are displayed
					Deliver_Home.verifyElementPresent(driver, ".panel--tracking__header", test);
					
					myWait(1000);
					
					Deliver_Home.ClearText(driver, "form-control", test);
					myWait(1000);
					
				}
				
			
			}
			

	}
	
	@Test(priority = 3)
	public void trackDeliveryGrid3() throws IOException {
			
			ExtentTest test = extent.createTest("TESTCASE 3 : Test Client Registration - TestCase 3");
			
			// Import excel sheet.
			
			String projectDir = System.getProperty("user.dir");
			String fileLocation = projectDir + testDataLoc ;
			
			System.out.println(fileLocation);
		
			File src=new File(fileLocation); 
			// Load the file.
			FileInputStream fis = new FileInputStream(src);
			// Load he workbook.
			workbook = new XSSFWorkbook(fis);
			// Load the sheet in which data is stored.
			sheet= workbook.getSheet("Test Data TMS");
		
			
			DesiredCapabilities dc=DesiredCapabilities.chrome();
			URL url = new URL("http://localhost:4444/wd/hub");
			RemoteWebDriver driver = new RemoteWebDriver(url,dc);
			driver.get(testSite);
				
			for(int i = 1;i<=sheet.getLastRowNum();i++) {
				
				cell = sheet.getRow(i).getCell(0);
				
				if(cell==null) {
					driver.quit();//close the driver if there is no more data to fetch			
				}else {
					cell.setCellType(CellType.STRING);
					String doNumber = cell.getStringCellValue();
					System.out.println(doNumber);
					
					Deliver_Home.EnterTextByClassName(driver, "form-control", doNumber, test);
					myWait(2000);
					Deliver_Home.ClickButton(driver, "Track Item", test);
					myWait(2000);
					
					
					//verify results are displayed
					Deliver_Home.verifyElementPresent(driver, ".panel--tracking__header", test);
					
					myWait(1000);
					
					Deliver_Home.ClearText(driver, "form-control", test);
					myWait(1000);
					
				}
				
			
			}
			

	}

	@Test(priority = 4)
	public void trackDeliveryGrid4() throws IOException {
			
			ExtentTest test = extent.createTest("TESTCASE 4 : Test Client Registration - TestCase 4");
			
			// Import excel sheet.
			
			String projectDir = System.getProperty("user.dir");
			String fileLocation = projectDir + testDataLoc ;
			
			System.out.println(fileLocation);
		
			File src=new File(fileLocation); 
			// Load the file.
			FileInputStream fis = new FileInputStream(src);
			// Load he workbook.
			workbook = new XSSFWorkbook(fis);
			// Load the sheet in which data is stored.
			sheet= workbook.getSheet("Test Data TMS");
		
			
			DesiredCapabilities dc=DesiredCapabilities.chrome();
			URL url = new URL("http://localhost:4444/wd/hub");
			RemoteWebDriver driver = new RemoteWebDriver(url,dc);
			driver.get(testSite);
				
			for(int i = 1;i<=sheet.getLastRowNum();i++) {
				
				cell = sheet.getRow(i).getCell(0);
				
				if(cell==null) {
					driver.quit();//close the driver if there is no more data to fetch			
				}else {
					cell.setCellType(CellType.STRING);
					String doNumber = cell.getStringCellValue();
					System.out.println(doNumber);
					
					Deliver_Home.EnterTextByClassName(driver, "form-control", doNumber, test);
					myWait(2000);
					Deliver_Home.ClickButton(driver, "Track Item", test);
					myWait(2000);
					
					
					//verify results are displayed
					Deliver_Home.verifyElementPresent(driver, ".panel--tracking__header", test);
					
					myWait(1000);
					
					Deliver_Home.ClearText(driver, "form-control", test);
					myWait(1000);
					
				}
				
			
			}
			

	}
	
	@Test(priority = 5)
	public void trackDeliveryGrid5() throws IOException {
			
			ExtentTest test = extent.createTest("TESTCASE 5 : Test Client Registration - TestCase 5");
			
			// Import excel sheet.
			
			String projectDir = System.getProperty("user.dir");
			String fileLocation = projectDir + testDataLoc ;
			
			System.out.println(fileLocation);
		
			File src=new File(fileLocation); 
			// Load the file.
			FileInputStream fis = new FileInputStream(src);
			// Load he workbook.
			workbook = new XSSFWorkbook(fis);
			// Load the sheet in which data is stored.
			sheet= workbook.getSheet("Test Data TMS");
		
			
			DesiredCapabilities dc=DesiredCapabilities.chrome();
			URL url = new URL("http://localhost:4444/wd/hub");
			RemoteWebDriver driver = new RemoteWebDriver(url,dc);
			driver.get(testSite);
				
			for(int i = 1;i<=sheet.getLastRowNum();i++) {
				
				cell = sheet.getRow(i).getCell(0);
				
				if(cell==null) {
					driver.quit();//close the driver if there is no more data to fetch			
				}else {
					cell.setCellType(CellType.STRING);
					String doNumber = cell.getStringCellValue();
					System.out.println(doNumber);
					
					Deliver_Home.EnterTextByClassName(driver, "form-control", doNumber, test);
					myWait(2000);
					Deliver_Home.ClickButton(driver, "Track Item", test);
					myWait(2000);
					
					
					//verify results are displayed
					Deliver_Home.verifyElementPresent(driver, ".panel--tracking__header", test);
					
					myWait(1000);
					
					Deliver_Home.ClearText(driver, "form-control", test);
					myWait(1000);
					
				}
				
			
			}
			

	}
	
	@Test(priority = 6)
	public void trackDeliveryGrid6() throws IOException {
			
			ExtentTest test = extent.createTest("TESTCASE 6 : Test Client Registration - TestCase 6");
			
			// Import excel sheet.
			
			String projectDir = System.getProperty("user.dir");
			String fileLocation = projectDir + testDataLoc ;
			
			System.out.println(fileLocation);
		
			File src=new File(fileLocation); 
			// Load the file.
			FileInputStream fis = new FileInputStream(src);
			// Load he workbook.
			workbook = new XSSFWorkbook(fis);
			// Load the sheet in which data is stored.
			sheet= workbook.getSheet("Test Data TMS");
		
			
			DesiredCapabilities dc=DesiredCapabilities.chrome();
			URL url = new URL("http://localhost:4444/wd/hub");
			RemoteWebDriver driver = new RemoteWebDriver(url,dc);
			driver.get(testSite);
				
			for(int i = 1;i<=sheet.getLastRowNum();i++) {
				
				cell = sheet.getRow(i).getCell(0);
				
				if(cell==null) {
					driver.quit();//close the driver if there is no more data to fetch			
				}else {
					cell.setCellType(CellType.STRING);
					String doNumber = cell.getStringCellValue();
					System.out.println(doNumber);
					
					Deliver_Home.EnterTextByClassName(driver, "form-control", doNumber, test);
					myWait(2000);
					Deliver_Home.ClickButton(driver, "Track Item", test);
					myWait(2000);
					
					
					//verify results are displayed
					Deliver_Home.verifyElementPresent(driver, ".panel--tracking__header", test);
					
					myWait(1000);
					
					Deliver_Home.ClearText(driver, "form-control", test);
					myWait(1000);
					
				}
				
			
			}
			

	}
	
	@Test(priority = 7)
	public void trackDeliveryGrid7() throws IOException {
			
			ExtentTest test = extent.createTest("TESTCASE 7 : Test Client Registration - TestCase 7");
			
			// Import excel sheet.
			
			String projectDir = System.getProperty("user.dir");
			String fileLocation = projectDir + testDataLoc ;
			
			System.out.println(fileLocation);
		
			File src=new File(fileLocation); 
			// Load the file.
			FileInputStream fis = new FileInputStream(src);
			// Load he workbook.
			workbook = new XSSFWorkbook(fis);
			// Load the sheet in which data is stored.
			sheet= workbook.getSheet("Test Data TMS");
		
			
			DesiredCapabilities dc=DesiredCapabilities.chrome();
			URL url = new URL("http://localhost:4444/wd/hub");
			RemoteWebDriver driver = new RemoteWebDriver(url,dc);
			driver.get(testSite);
				
			for(int i = 1;i<=sheet.getLastRowNum();i++) {
				
				cell = sheet.getRow(i).getCell(0);
				
				if(cell==null) {
					driver.quit();//close the driver if there is no more data to fetch			
				}else {
					cell.setCellType(CellType.STRING);
					String doNumber = cell.getStringCellValue();
					System.out.println(doNumber);
					
					Deliver_Home.EnterTextByClassName(driver, "form-control", doNumber, test);
					myWait(2000);
					Deliver_Home.ClickButton(driver, "Track Item", test);
					myWait(2000);
					
					
					//verify results are displayed
					Deliver_Home.verifyElementPresent(driver, ".panel--tracking__header", test);
					
					myWait(1000);
					
					Deliver_Home.ClearText(driver, "form-control", test);
					myWait(1000);
					
				}
				
			
			}
			

	}
	
	@Test(priority = 8)
	public void trackDeliveryGrid8() throws IOException {
			
			ExtentTest test = extent.createTest("TESTCASE 8 : Test Client Registration - TestCase 8");
			
			// Import excel sheet.
			
			String projectDir = System.getProperty("user.dir");
			String fileLocation = projectDir + testDataLoc ;
			
			System.out.println(fileLocation);
		
			File src=new File(fileLocation); 
			// Load the file.
			FileInputStream fis = new FileInputStream(src);
			// Load he workbook.
			workbook = new XSSFWorkbook(fis);
			// Load the sheet in which data is stored.
			sheet= workbook.getSheet("Test Data TMS");
		
			
			DesiredCapabilities dc=DesiredCapabilities.chrome();
			URL url = new URL("http://localhost:4444/wd/hub");
			RemoteWebDriver driver = new RemoteWebDriver(url,dc);
			driver.get(testSite);
				
			for(int i = 1;i<=sheet.getLastRowNum();i++) {
				
				cell = sheet.getRow(i).getCell(0);
				
				if(cell==null) {
					driver.quit();//close the driver if there is no more data to fetch			
				}else {
					cell.setCellType(CellType.STRING);
					String doNumber = cell.getStringCellValue();
					System.out.println(doNumber);
					
					Deliver_Home.EnterTextByClassName(driver, "form-control", doNumber, test);
					myWait(2000);
					Deliver_Home.ClickButton(driver, "Track Item", test);
					myWait(2000);
					
					
					//verify results are displayed
					Deliver_Home.verifyElementPresent(driver, ".panel--tracking__header", test);
					
					myWait(1000);
					
					Deliver_Home.ClearText(driver, "form-control", test);
					myWait(1000);
					
				}
				
			
			}
			

	}
	
	@Test(priority = 9)
	public void trackDeliveryGrid9() throws IOException {
			
			ExtentTest test = extent.createTest("TESTCASE 9 : Test Client Registration - TestCase 9");
			
			// Import excel sheet.
			
			String projectDir = System.getProperty("user.dir");
			String fileLocation = projectDir + testDataLoc ;
			
			System.out.println(fileLocation);
		
			File src=new File(fileLocation); 
			// Load the file.
			FileInputStream fis = new FileInputStream(src);
			// Load he workbook.
			workbook = new XSSFWorkbook(fis);
			// Load the sheet in which data is stored.
			sheet= workbook.getSheet("Test Data TMS");
		
			
			DesiredCapabilities dc=DesiredCapabilities.chrome();
			URL url = new URL("http://localhost:4444/wd/hub");
			RemoteWebDriver driver = new RemoteWebDriver(url,dc);
			driver.get(testSite);
				
			for(int i = 1;i<=sheet.getLastRowNum();i++) {
				
				cell = sheet.getRow(i).getCell(0);
				
				if(cell==null) {
					driver.quit();//close the driver if there is no more data to fetch			
				}else {
					cell.setCellType(CellType.STRING);
					String doNumber = cell.getStringCellValue();
					System.out.println(doNumber);
					
					Deliver_Home.EnterTextByClassName(driver, "form-control", doNumber, test);
					myWait(2000);
					Deliver_Home.ClickButton(driver, "Track Item", test);
					myWait(2000);
					
					
					//verify results are displayed
					Deliver_Home.verifyElementPresent(driver, ".panel--tracking__header", test);
					
					myWait(1000);
					
					Deliver_Home.ClearText(driver, "form-control", test);
					myWait(1000);
					
				}
				
			
			}
			

	}
	
	@Test(priority = 10)
	public void trackDeliveryGrid10() throws IOException {
			
			ExtentTest test = extent.createTest("TESTCASE 10 : Test Client Registration - TestCase 10");
			
			// Import excel sheet.
			
			String projectDir = System.getProperty("user.dir");
			String fileLocation = projectDir + testDataLoc ;
			
			System.out.println(fileLocation);
		
			File src=new File(fileLocation); 
			// Load the file.
			FileInputStream fis = new FileInputStream(src);
			// Load he workbook.
			workbook = new XSSFWorkbook(fis);
			// Load the sheet in which data is stored.
			sheet= workbook.getSheet("Test Data TMS");
		
			
			DesiredCapabilities dc=DesiredCapabilities.chrome();
			URL url = new URL("http://localhost:4444/wd/hub");
			RemoteWebDriver driver = new RemoteWebDriver(url,dc);
			driver.get(testSite);
				
			for(int i = 1;i<=sheet.getLastRowNum();i++) {
				
				cell = sheet.getRow(i).getCell(0);
				
				if(cell==null) {
					driver.quit();//close the driver if there is no more data to fetch			
				}else {
					cell.setCellType(CellType.STRING);
					String doNumber = cell.getStringCellValue();
					System.out.println(doNumber);
					
					Deliver_Home.EnterTextByClassName(driver, "form-control", doNumber, test);
					myWait(2000);
					Deliver_Home.ClickButton(driver, "Track Item", test);
					myWait(2000);
					
					
					//verify results are displayed
					Deliver_Home.verifyElementPresent(driver, ".panel--tracking__header", test);
					
					myWait(1000);
					
					Deliver_Home.ClearText(driver, "form-control", test);
					myWait(1000);
					
				}
				
			
			}
			

	}


}

