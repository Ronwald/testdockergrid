package testCases;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteException;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Setup_DockerGrid {
	
	String [] cmd = {
			"/bin/bash",
			"docker-compose -f docker-compose.yaml up -d"
	};
	
	
	@BeforeTest
	public void startDockerGrid() throws IOException, InterruptedException {
		//("cmd /c start start_dockergrid.bat")
		Runtime.getRuntime().exec("cmd /c start start_dockergrid.bat");
		Thread.sleep(15000);
		
	}
	
	
	@AfterTest
	public void stopDockerGrid() throws IOException, InterruptedException {
		
		Runtime.getRuntime().exec("cmd /c start stop_dockergrid.bat");
		Thread.sleep(5000);
		//close all command prompt
		Runtime.getRuntime().exec("taskkill /f /im cmd.exe");
		
	}
	
	
	

}
