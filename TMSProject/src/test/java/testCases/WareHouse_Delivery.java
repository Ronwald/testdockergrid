package testCases;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import pageObjects.DP_ControllerReport;
import pageObjects.DP_Home;
import pageObjects.DP_Login;
import pageObjects.DP_Tools;
import utilities.MyUtilities;


public class WareHouse_Delivery extends MyUtilities {
	
	
	public final String MLNumber = "ML3204074";
	public final String username = "ronwald.sandoval@urbanfox.asia";
	public final String password = "Password1";
	public final String driverUID = "401038";
	public final String driverUsername = "vishal.paliwal+0@urbanfox.asia";
	public final String driverPassword = "Password1";
	
	

	
	@Test(priority = 1)
	public void createBundle() throws IOException {
		
		//initialize the report
		ExtentTest test = extent.createTest("TESTCASE XX : Test Successful Creation of Bundle and Assign to Driver for a delivery order");
		
		//set the chrome driver, launch Chrome browser and go to test site,  and maximize the window
		System.setProperty("webdriver.chrome.driver","C:/Chrome Driver/chromedriver_win32/chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://dp-stag.urbanfox.asia/");
		driver.manage().window().maximize();
		//login to the test site
		DP_Login.LoginButton(driver).click();
		DP_Login.TextField(driver, "username").sendKeys(username);
		DP_Login.TextField(driver, "password").sendKeys(password);
		DP_Login.SignIn(driver).click();
		test.log(Status.PASS, "User is successfully logged in to DP page");
		
	
		myWait(2000);
        DP_Home.ClickLink(driver, "Tools", test);
        myWait(2000);
        DP_Home.ClickLink(driver, "Transaction Search", test);
        myWait(2000);
		DP_Tools.EnterText(driver, "form-control", MLNumber, test);
		myWait(2000);
		DP_Tools.ClickButton(driver, "Search", test);
		myWait(2000);	
		DP_Tools.clickCaret(driver, "^", test);
		
	
	    //The ML details are displayed on a new tab, so we need to switch the control
		DP_Tools.switchTab(driver);
		myWait(2000);

		
		//verify ML page is displayed. If displayed, pass the test, else fail the test
		if(driver.getTitle().contains("ML")) {
		    //Pass
			test.log(Status.PASS, driver.getTitle());
		    test.log(Status.PASS, "Warehouse Delivery orer details are displayed");
		}    
		
		else {
		    //Fail
			test.log(Status.FAIL, driver.getTitle());
			test.log(Status.FAIL, "Warehouse Delivery orer details are Not displayed");
			//take screenshot if fail so its easier to investigate
			takeScreenshot(driver, test);
			
		}
		myWait(1000);
		
		DP_Home.ClickLink(driver, "bundle, schedule & more", test);

		
		DP_Tools.switchSpecificTab(driver, "Controller Report");

		myWait(1000);
		DP_ControllerReport.clickCheckBox(driver, test);
		DP_ControllerReport.clickButton(driver, "Create", test);
		DP_ControllerReport.clickLinkText(driver, "Bundle", test);
		myWait(1000);
		DP_ControllerReport.enterText(driver, "1", test);
		myWait(2000);
		DP_ControllerReport.clickButton(driver, "Next", test);
		myWait(2000);
		
		driver.findElement(By.cssSelector("div > div > input")).sendKeys(driverUID);
		myWait(2000);
		DP_ControllerReport.clickButton(driver, "Next", test);
		myWait(2000);
		DP_ControllerReport.clickButton(driver, "Done", test);
		myWait(3000);
		
		
		//verify bundle creation confirmation page is displayed
		String expMessage = "has been created.";
		String actualMessage = driver.findElement(By.xpath("//div[@class='modal-body']/div")).getText();
		
		System.out.println(expMessage);
		System.out.println(actualMessage);
		
		if(actualMessage.trim().contains(expMessage.trim())) {
			//Pass
		    test.log(Status.PASS, "Bundle is created for the delivery order");
		    test.log(Status.INFO, actualMessage);
			
		}else {
			//Fail
			test.log(Status.FAIL, "Bundle is not created for the delivery order");
			takeScreenshot(driver, test);
			
		}
		
			
		//close the driver
		driver.quit();
		
	}
	
	
	@Test(priority = 0)
	public void orderProcessing() throws IOException {
		
		//initialize the report
		ExtentTest test = extent.createTest("TESTCASE XX : Test Successful Scan Transaction to Order Processing");
		
		//set the chrome driver, launch Chrome browser and go to test site,  and maximize the window
		System.setProperty("webdriver.chrome.driver","C:/Chrome Driver/chromedriver_win32/chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://dp-stag.urbanfox.asia/");
		driver.manage().window().maximize();
		//login to the test site
		DP_Login.LoginButton(driver).click();
		DP_Login.TextField(driver, "username").sendKeys(username);
		DP_Login.TextField(driver, "password").sendKeys(password);
		DP_Login.SignIn(driver).click();
		test.log(Status.PASS, "User is successfully logged in to DP page");
		
	
		myWait(2000);
        DP_Home.ClickLink(driver, "Tools", test);
        myWait(2000);
        DP_Home.ClickLink(driver, "Scan Transaction To Order Processing", test);
        myWait(2000);
		DP_Tools.EnterText(driver, "form-control", MLNumber, test);
		myWait(2000);
		DP_Tools.ClickButton(driver, "Scan Order Processing", test);
		myWait(5000);	
		
		//verify bundle creation confirmation page is displayed
		String expMessage = "Order Processing";
		String actualMessage = driver.findElement(By.xpath("//h3")).getText();
		
		System.out.println(expMessage);
		System.out.println(actualMessage);
		
		if(actualMessage.trim().contains(expMessage.trim())) {
			//Pass
		    test.log(Status.PASS, "Delivery order is Successfully Scanned to Order Processing");
		    test.log(Status.INFO, actualMessage);
			
		}else {
			//Fail
			test.log(Status.FAIL, "Delivery order is Not Successfully Scanned to Order Processing");
			takeScreenshot(driver, test);
			
		}
		
		//close the driver
		driver.quit();
		
	}
	
	
	@Test(priority = 2)
	public void departedHub() throws IOException {
		
		//initialize the report
		ExtentTest test = extent.createTest("TESTCASE XX : Test Successful Scan Transaction to Departed Hub");
		
		//set the chrome driver, launch Chrome browser and go to test site,  and maximize the window
		System.setProperty("webdriver.chrome.driver","C:/Chrome Driver/chromedriver_win32/chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://dp-stag.urbanfox.asia/");
		driver.manage().window().maximize();
		//login to the test site
		DP_Login.LoginButton(driver).click();
		DP_Login.TextField(driver, "username").sendKeys(driverUsername);
		DP_Login.TextField(driver, "password").sendKeys(driverPassword);
		DP_Login.SignIn(driver).click();
		test.log(Status.PASS, "User is successfully logged in to DP page");
		
	
		myWait(2000);
        DP_Home.ClickLink(driver, "Tools", test);
        myWait(2000);
        DP_Home.ClickLink(driver, "Scan Transaction To Departed Hub", test);
        myWait(2000);
		DP_Tools.EnterText(driver, "form-control", MLNumber, test);
		myWait(2000);
		DP_Tools.ClickButton(driver, "Scan Departed Hub", test);
		myWait(3000);	
		
		//verify bundle creation confirmation page is displayed
		String expMessage = "Departed Hub";
		String actualMessage = driver.findElement(By.xpath("//h3")).getText();
		
		
		if(actualMessage.trim().contains(expMessage.trim())) {
			//Pass
		    test.log(Status.PASS, "Delivery order is Successfully Scanned to Departed Hub");
		    test.log(Status.INFO, actualMessage);
			
		}else {
			//Fail
			test.log(Status.FAIL, "Delivery order is Not Successfully Scanned to Departed Hub");
			takeScreenshot(driver, test);
			
		}
		
		//close the driver
		driver.quit();
		
	}
		
	
	
	
	

}
