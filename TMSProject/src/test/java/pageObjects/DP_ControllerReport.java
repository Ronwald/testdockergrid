package pageObjects;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import utilities.MyUtilities;

public class DP_ControllerReport {
	
	public static ExtentHtmlReporter htmlReporter;
	public static ExtentReports extent;
	public static ExtentTest test;
	private static WebElement element = null;
	
	
	public static void clickCheckBox(WebDriver driver, ExtentTest test) throws IOException {
					
	    //List<WebElement> elements = driver.findElements(By.xpath("xpath=(//input[@type='checkbox'])[23]"));
	    List<WebElement> elements = driver.findElements(By.xpath("//td/input"));
	        
	    
	    if(elements.size()>0) {
	    	elements.get(0).click();
	    	test.log(Status.PASS, "Step passed. Checkbox is clicked");
	    }else {
	    	test.log(Status.FAIL, "Step failed. Checkbox is not clicked / not displayed.");
	    	MyUtilities.takeScreenshot(driver, test);   	
	    	//driver.quit();
	    	   	
	    }
	    
	}
	
	public static void clickButton(WebDriver driver, String buttonName, ExtentTest test) throws IOException {
		
	    //List<WebElement> elements = driver.findElements(By.xpath("xpath=(//input[@type='checkbox'])[23]"));
	    List<WebElement> elements = driver.findElements(By.xpath("//button[contains(.,'"  + buttonName + "')]"));
	        	   		    	
	    
	    if(elements.size()>0) {
	    	elements.get(0).click();
	    	test.log(Status.PASS, "Step passed. Button : " + buttonName + " is clicked");
	    }else {
	    	test.log(Status.FAIL, "Step failed. Button : " + buttonName + " is not clicked / displayed.");
	    	MyUtilities.takeScreenshot(driver, test);   	
	    	//driver.quit();
	    	   	
	    }
	    
	}
	
	
	//this method is used to click link texts in the controller report
	public static void clickLinkText(WebDriver driver, String linkText, ExtentTest test) throws IOException {
		
	    //List<WebElement> elements = driver.findElements(By.xpath("xpath=(//input[@type='checkbox'])[23]"));
	    List<WebElement> elements = driver.findElements(By.xpath("//a[contains(text(),'"  + linkText + "')]"));
	        	   		    		    
	    if(elements.size()>0) {
	    	elements.get(0).click();
	    	test.log(Status.PASS, "Step passed. Link text : " + linkText + " is clicked");
	    }else {
	    	test.log(Status.FAIL, "Step failed. Link text : " + linkText + " is not clicked / displayed.");
	    	MyUtilities.takeScreenshot(driver, test);   	
	    	//driver.quit();
	    	   	
	    }
	    
	}
	
	
	public static void enterText(WebDriver driver, String bundleNum, ExtentTest test) throws IOException {
		
	    List<WebElement> elements = driver.findElements(By.cssSelector("input:nth-child(3)"));
	        	   		    		    
	    if(elements.size()>0) {
	    	elements.get(0).sendKeys(bundleNum);
	    	test.log(Status.PASS, "Step passed. Number of Deliveries : " + bundleNum + " is entered.");
	    }else {
	    	test.log(Status.FAIL, "Step failed. Number of Deliveries : " + bundleNum + " is not entered.");
	    	MyUtilities.takeScreenshot(driver, test);   	
	    	//driver.quit();
	    	   	
	    }
	    
	}
	
	
	
	

}
